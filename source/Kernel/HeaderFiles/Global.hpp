// Global.hpp
// Copyright (c) 2013 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under the Apache License Version 2.0.


#pragma once
#include <stdint.h>
#include <HardwareAbstraction.hpp>
#include <Multiboot.hpp>
#include <Bootscreen.hpp>
#include <ConfigFile.hpp>
#include <Console.hpp>
#include <Time.hpp>
#include <Mutexes.hpp>
#include <Kernel.hpp>

#include <List.hpp>
#include <SimpleStringMap.hpp>
#include <CircularBuffer.hpp>






