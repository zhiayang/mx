# [mx] #

The fifth rewrite of what never was, OrionOS.



## Current Features: ##

- Pretty bare now, features will get ported over from Orion-X4 as I get to it.
- Long mode (x86-64)
- BGA Support, works in QEMU, VirtualBox and Bochs.
- Working IPC system.
- Basic networking support (DHCP, DNS, IPv4, TCP/UDP)




