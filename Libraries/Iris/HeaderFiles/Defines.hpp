// userspace/Defines.hpp
// Copyright (c) 2013 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under the Apache License Version 2.0.



#include <stdint.h>

#define va_list					__builtin_va_list
#define va_start				__builtin_va_start
#define va_end					__builtin_va_end
#define va_arg					__builtin_va_arg
